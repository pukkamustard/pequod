# Vocabulary used by Pequod

This document describes the terms used by Pequod.

Pequod uses following existing vocabularies:

- [ActivityStreams Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary/)
- [The Music Ontology](http://musicontology.com/specification/)
- [FOAF Vocabulary](http://xmlns.com/foaf/spec/)
- [DCMI Metadata Terms](https://dublincore.org/specifications/dublin-core/dcmi-terms/)

# Scrobble

A scrobble is the information that records when a person listened to a certain piece of music.

In Pequod a scrobble is described with two classes:

1. The ActivityStreams `Listen` activity
2. The Music Ontology `Track` class

A Pequod scrobble looks like this (in RDF/Turtle):

```
@prefix as: <https://www.w3.org/ns/activitystreams#> .
@prefix mo: <http://purl.org/ontology/mo/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA> 
  a as:Listen ;
  as:actor <xmpp:user@strawberry.local> ;
  as:object <urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA#track> .
  
<urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA#track>
  a mo:Track ;
  dcterms:creator "Funki Porcini" ;
  dcterms:title "Back Home" ;
  mo:musicbrainz <urn:uuid:a9dae29a-3f23-4c4b-804d-e125d4582adf> ;
  mo:release <urn:uuid:0f028066-5891-322e-ad8d-6aa588063a2e> ;
  foaf:maker <urn:uuid:2adb429d-e39c-467b-b175-3f40440ff630> .
```

The track is primarily described with [MusicBrainz identifiers](https://musicbrainz.org/doc/MusicBrainz_Identifier).

The predicates `dcterms:creator` and `dcterms:title` are included so that consumers can display minimal information without consulting MusicBrainz.

We follow the best practices for usage of `foaf:maker` nd `dcterms:creator` as described [here](https://web.archive.org/web/20100108074823/wiki.foaf-project.org/w/UsingDublinCoreCreator).
 
Note that over-the-wire we use the RDF/XML serialization. The scrobble in RDF/XML:

```
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:as="https://www.w3.org/ns/activitystreams#"
		 xmlns:mo="http://purl.org/ontology/mo/"
		 xmlns:dcterms="http://purl.org/dc/terms/"
		 xmlns:foaf="http://xmlns.com/foaf/0.1/">

<rdf:Description rdf:about="urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA">
    <rdf:type rdf:resource="https://www.w3.org/ns/activitystreams#Listen"/>
    <as:actor rdf:resource="xmpp:user@strawberry.local"/>
    <as:object rdf:resource="urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA#track"/>
    <as:startTime rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
    2022-01-24T12:56:31-00:00
    </as:startTime>
</rdf:Description>

<rdf:Description rdf:about="urn:blake2b:E6ZDJ4HL43HVEMSR7CJBJ4SLCVKEAWPJ26RJSPNNGMMZJSE64LEP3AFA7IYL7IAOMKO5XPYV7F6ZANU6HOI7R5NYBXF6IV4B27IYAMA#track">
    <dcterms:creator>
    Funki Porcini
    </dcterms:creator>
    <dcterms:title>
    Back Home
    </dcterms:title>
    <mo:musicbrainz rdf:resource="urn:uuid:a9dae29a-3f23-4c4b-804d-e125d4582adf"/>
    <mo:release rdf:resource="urn:uuid:0f028066-5891-322e-ad8d-6aa588063a2e"/>
    <rdf:type rdf:resource="http://purl.org/ontology/mo/Track"/>
    <foaf:maker rdf:resource="urn:uuid:2adb429d-e39c-467b-b175-3f40440ff630"/>
</rdf:Description>
</rdf:RDF>
```


