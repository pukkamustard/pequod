(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Cmdliner

type t = {
  jid : Xmppl.Jid.t;
  password : string;
  disable_ssl : bool;
  port : int;
}

let docs = Manpage.s_common_options

let log_level_t =
  let doc = " Set the log level" in
  let env = Cmd.Env.info "PEQUOD_LOG_LEVEL" ~doc in
  Logs_cli.level ~env ()

let style_renderer_t = Fmt_cli.style_renderer ()

let jid_t =
  let doc = "Connect to XMPP server with JID" in
  Term.(
    const (fun s -> Option.get @@ Xmppl.Jid.of_string s)
    $ Arg.(required & opt (some string) None & info [ "jid" ] ~docv:"JID" ~doc))

let password_t =
  let doc = "Password for authenticating with XMPP server" in
  Arg.(
    required
    & opt (some string) None
    & info [ "p"; "password" ] ~docv:"PASSWORD" ~doc)

let disable_ssl_t =
  let doc =
    "Disable SSL when connecting to XMPP server (this is only useful for \
     development with a local XMPP server)"
  in
  Arg.(value & flag & info [ "disable-ssl" ] ~doc)

let port_t =
  let doc = "XMPP server port to connect to." in
  Arg.(value & opt int 5223 & info [ "port" ] ~docv:"PORT" ~doc)

let term =
  Term.(
    const (fun style_renderer log_level jid password disable_ssl port ->
        (* setup tty *)
        Fmt_tty.setup_std_outputs ?style_renderer ();

        (* setup logger  *)
        Logs.set_reporter (Logs_fmt.reporter ());
        Logs.set_level log_level;

        { jid; password; disable_ssl; port })
    $ style_renderer_t $ log_level_t $ jid_t $ password_t $ disable_ssl_t
    $ port_t)
