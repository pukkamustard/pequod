(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react
open Archi_lwt

(* Setup logging *)
let src = Logs.Src.create "Pequod.Mpd"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

(* MPD Protocol *)

type connection = { ic : Lwt_io.input_channel; oc : Lwt_io.output_channel }

let connect (_config : Configuration.t) =
  let host = Unix.inet_addr_loopback in
  let port = 6600 in
  let fd = Lwt_unix.(socket PF_INET SOCK_STREAM 0) in
  let* () =
    Log.debug (fun m ->
        m "Connecting to MPD at %s:%d" (Unix.string_of_inet_addr host) port)
  in
  let* () = Lwt_unix.(connect fd (ADDR_INET (host, port))) in

  (* open socket as input/output channels *)
  let ic =
    Lwt_io.of_fd ~close:(fun () -> Lwt_unix.close fd) ~mode:Lwt_io.input fd
  in
  let oc =
    Lwt_io.of_fd ~close:(fun () -> Lwt_unix.close fd) ~mode:Lwt_io.output fd
  in

  return { ic; oc }

let ok_re = Str.regexp "^OK"
let sep_re = Str.regexp ": "

module Response = Map.Make (String)

let read_response { ic; _ } =
  let rec read response =
    let* line = Lwt_io.read_line ic in
    let* () = Log.debug (fun m -> m "MPD RECV: %s" line) in
    if Str.string_match ok_re line 0 then return response
    else
      match Str.bounded_split sep_re line 2 with
      | [ key; value ] -> read @@ Response.add key value response
      | _ -> fail_with "Failure while parsing MPD response"
  in
  read Response.empty

let write t cmd =
  let* () = Log.debug (fun m -> m "MPD SEND: %s" cmd) in
  Lwt_io.write_line t.oc cmd

let read_hello t = read_response t >|= fun _ -> ()
let status t = write t "status" >>= fun () -> read_response t

let playing_songid t =
  let* status = status t in
  match Response.find_opt "state" status with
  | Some "play" -> return @@ Response.find_opt "songid" status
  | Some _ -> return_none
  | None -> return_none

let playlistid t songid =
  write t ("playlistid " ^ songid) >>= fun () -> read_response t

let get_playing_now t =
  playing_songid t >>= function
  | Some songid -> playlistid t songid >>= return_some
  | None -> return_none

let idle_player t =
  write t "idle player" >>= fun () ->
  read_response t >|= fun _response -> ()

(* Component state *)

type playing = string Response.t option
type t = { mpd : connection; playing : playing S.t; listener : unit Lwt.t }

let start (config : Configuration.t) =
  let* mpd = connect config in

  (* read the initial hello message *)
  let* () = read_hello mpd in

  (* get what MPD is currently playing *)
  let* playing_now = get_playing_now mpd in

  (* create a signal holding the song that MPD is playing *)
  let playing, push_playing =
    S.create ~eq:(Option.equal @@ Response.equal @@ String.equal) playing_now
  in

  let listener =
    let rec loop mpd =
      Lwt_result.catch @@ idle_player mpd >>= function
      | Ok () -> get_playing_now mpd >|= push_playing >>= fun () -> loop mpd
      | Error e ->
          (* TODO reconnnect with exponential backoff *)
          (* TODO gracefully handle stop *)
          Log.err (fun m ->
              m "Error while talking to MPD: %s" @@ Printexc.to_string e)
    in
    loop mpd
  in

  let* () = Log.info (fun m -> m "Connected to MPD.") in

  return_ok { mpd; playing; listener }

let stop t =
  Lwt_io.close t.mpd.ic >>= fun () ->
  Log.info (fun m -> m "Connection to MPD closed")

let component = Component.make ~start ~stop

(* Component API *)

let playing t = t.playing
