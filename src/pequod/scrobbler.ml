(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react
open Archi_lwt

(* Setup logging *)
let src = Logs.Src.create "Pequod.Scrobbler"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

module Graph = struct
  type t = Rdf.Graph.t

  let listen_activities graph =
    let listen = Vocabulary.as' "Listen" |> Rdf.Triple.Object.of_iri in
    Rdf.Graph.subjects graph
    |> Seq.filter (fun subject ->
           match
             Rdf.Graph.functional_property subject
               (Rdf.Triple.Predicate.of_iri @@ Rdf.Namespace.rdf "type")
               graph
           with
           | Some type' when Rdf.Triple.Object.equal type' listen -> true
           | _ -> false)
    |> List.of_seq
    |> List.sort (fun listen_a listen_b ->
           let start_time =
             Vocabulary.as' "startTime" |> Rdf.Triple.Predicate.of_iri
           in
           let st_a = Rdf.Graph.functional_property listen_a start_time graph in
           let st_b = Rdf.Graph.functional_property listen_b start_time graph in
           Option.compare Rdf.Triple.Object.compare st_a st_b)

  let option_bind f m = Option.bind m f

  let pp_track graph ppf subject =
    let artist =
      Rdf.Graph.functional_property subject
        (Vocabulary.dc_terms "creator" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_literal
      |> Option.map Rdf.Literal.canonical
    in

    let title =
      Rdf.Graph.functional_property subject
        (Vocabulary.dc_terms "title" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_literal
      |> Option.map Rdf.Literal.canonical
    in
    let musicbrainz =
      Rdf.Graph.functional_property subject
        (Vocabulary.mo "musicbrainz" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_iri
      |> Option.map (fun iri ->
             match String.split_on_char ':' (Rdf.Iri.to_string iri) with
             | [ _; _; uuid ] ->
                 "https://musicbrainz.org/track/" ^ uuid |> Rdf.Iri.of_string
             | _ -> iri)
    in
    Fmt.(
      pf ppf "@[%a - %a %a@]" (option string) artist (option string) title
        (option @@ styled `Faint @@ parens Rdf.Iri.pp)
        musicbrainz)

  let of_rfc3339 s =
    match Ptime.of_rfc3339 s with Ok (ptime, _, _) -> Some ptime | _ -> None

  let pp_listen graph ppf subject =
    let track =
      Rdf.Graph.functional_property subject
        (Vocabulary.as' "object" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_iri
      |> Option.map Rdf.Triple.Subject.of_iri
    in
    let actor =
      Rdf.Graph.functional_property subject
        (Vocabulary.as' "actor" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_iri
    in
    let start_time =
      Rdf.Graph.functional_property subject
        (Vocabulary.as' "startTime" |> Rdf.Triple.Predicate.of_iri)
        graph
      |> Option.map Rdf.Triple.Object.to_term
      |> option_bind Rdf.Term.to_literal
      |> Option.map Rdf.Literal.canonical
      |> option_bind of_rfc3339
    in
    Fmt.(
      pf ppf "@[%a %a: %a@]"
        (option @@ styled (`Fg `Cyan) Rdf.Iri.pp)
        actor
        (option @@ styled `Faint @@ parens @@ Ptime.pp)
        start_time
        (option @@ pp_track graph)
        track)
end

let get_graph ?jid xmpp =
  let* jid =
    match jid with Some jid -> return jid | None -> Xmpp.Client.jid xmpp
  in

  let item_parser =
    Xmlc.Parser.(
      element (Xmpp.PubSub.Namespace.pubsub "item") (fun _ ->
          Xmlc.Tree.parser >>| fun tree ->
          Xmlc.Tree.to_seq tree |> Rdf_xml.parse_to_graph))
  in

  let parser =
    Xmlc.Parser.(
      element (Xmpp.PubSub.Namespace.pubsub "pubsub") (fun _ ->
          element (Xmpp.PubSub.Namespace.pubsub "items") (fun _ ->
              many item_parser
              >>| List.fold_left Rdf.Graph.union Rdf.Graph.empty)))
  in

  Xmpp.retrieve xmpp jid >>= fun result ->
  match result.payload with
  | Some payload -> Xmlc.Tree.parse parser payload
  | None -> return Rdf.Graph.empty

let timeout ~duration f =
  Lwt.pick [ (Lwt_unix.sleep duration >>= fun () -> fail_with "Timeout"); f () ]

let scrobble ~xmpp mpd_response =
  let* jid = Xmpp.Client.jid xmpp in
  let actor =
    Rdf.Iri.of_string @@ "xmpp:" ^ Xmpp.Jid.to_string @@ Xmpp.Jid.bare jid
  in

  match Vocabulary.Track.of_mpd_response mpd_response with
  | Some track -> (
      let listen = Vocabulary.Scrobble.make ~actor track in

      (* Serialize graph to XML *)
      let* graph_xml = listen |> Vocabulary.Scrobble.to_xml in

      let id = Rdf.Iri.to_string @@ Vocabulary.Scrobble.id listen in

      (Lwt_result.catch @@ timeout ~duration:2.0
      @@ fun () -> Xmpp.publish xmpp ~id graph_xml)
      >>= function
      | Ok _ ->
          Log.info (fun m ->
              m "Scrobbled track %a to XMPP." Vocabulary.Track.pp track)
      | Error e ->
          Log.err (fun m ->
              m "Failed to scrobble track %a to XMPP: %s" Vocabulary.Track.pp
                track
              @@ Printexc.to_string e))
  | None -> Log.info (fun m -> m "No Musicbrainz id found to scrobble.")

(* Component state *)

let start _config xmpp mpd =
  let _ =
    S.changes (Mpd.playing mpd)
    |> E.map_s (function
         | Some playing -> scrobble ~xmpp playing
         | None -> Log.info (fun m -> m "MPD stopped"))
    |> E.keep
  in

  (* let* () = Log.info (fun m -> m "Pequod started. %a" Xmpp.Jid.pp jid) in *)
  (* let* _ = Xmpp.publish xmpp () in *)
  return_ok ()

let stop () = Log.info (fun m -> m "Pequod stoping")

let component =
  Component.using ~start ~stop ~dependencies:[ Xmpp.component; Mpd.component ]
