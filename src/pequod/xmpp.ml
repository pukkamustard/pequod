(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Archi_lwt

(* Setup logging *)
let src = Logs.Src.create "Pequod.Xmpp"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

(* XMPP Client *)

module Client = Xmppl_unix.Client
module Jid = Xmppl.Jid
module PubSub = Xmppl_pubsub.Make (Client)

(* ActivityStreams PEP node *)

let as_node = "net.openengiadina.xmpp.activitystreams"

(* Enable persistence on the ActivityStreams PEP node *)
let configure_pep_node client =
  let* jid = Client.jid client >|= Jid.bare in

  (* let* () =
   *   Client.iq_get client ~to':jid
   *     Xmlc.Tree.(
   *       make_element
   *         ~attributes:
   *           [
   *             Xmlc.Namespace.default_xmlns_declaration PubSub.Namespace.owner_ns;
   *           ]
   *         ~children:
   *           [
   *             make_element
   *               ~attributes:[ (PubSub.Namespace.owner "node", as_node) ]
   *               (PubSub.Namespace.owner "configure");
   *           ]
   *         (PubSub.Namespace.owner "pubsub"))
   *   >>= fun result ->
   *   Log.debug (fun m -> m "%a" Xmppl.Stanza.Iq.pp_result result)
   * in *)
  let data_ns = "jabber:x:data" in
  let data local = (data_ns, local) in

  (* TODO: Abstract this into a nice Xmppl_data_forms module. What you see here is low-level hacking. *)
  let config =
    Xmlc.Tree.(
      make_element
        ~attributes:
          [
            Xmlc.Namespace.default_xmlns_declaration data_ns;
            (data "type", "submit");
          ]
        ~children:
          [
            make_element
              ~attributes:[ (data "var", "pubsub#max_items") ]
              ~children:
                [ make_element ~children:[ make_data "256" ] (data "value") ]
              (data "field");
            make_element
              ~attributes:[ (data "var", "pubsub#persist_items") ]
              ~children:
                [ make_element ~children:[ make_data "1" ] (data "value") ]
              (data "field");
          ]
        (data "x"))
  in

  Lwt_result.catch
  @@ Client.iq_set client ~to':jid
       Xmlc.Tree.(
         make_element
           ~attributes:
             [
               Xmlc.Namespace.default_xmlns_declaration
                 PubSub.Namespace.owner_ns;
             ]
           ~children:
             [
               make_element
                 ~attributes:[ (PubSub.Namespace.owner "node", as_node) ]
                 ~children:[ config ]
                 (PubSub.Namespace.owner "configure");
             ]
           (PubSub.Namespace.owner "pubsub"))
  >>= function
  | Ok _ ->
      Log.info (fun m ->
          m "Configured XMPP PEP node %s to persist 256 items." as_node)
  | Error e ->
      Log.warn (fun m ->
          m "Could not configure XMPP PEP node (%s)." (Printexc.to_string e))

(* Component state *)

type t = Client.t

let start (configuration : Configuration.t) =
  let* client =
    Client.create
      {
        Xmppl_unix.default_options with
        port = configuration.port;
        disable_ssl = configuration.disable_ssl;
      }
      ~credentials:(`JidPassword (configuration.jid, configuration.password))
  in
  Lwt_result.catch @@ Client.connect client >>= function
  | Ok () ->
      let* () = configure_pep_node client in
      return_ok client
  | Error exn -> return_error @@ `Msg (Printexc.to_string exn)

let stop client = Client.disconnect client
let component = Component.make ~start ~stop

(* Component API *)

let publish client ~id xml =
  let* jid = Client.jid client in
  let item =
    Xmlc.Tree.(
      make_element
        ~attributes:[ (("", "id"), id) ]
        ~children:[ xml ]
        (PubSub.Namespace.pubsub "item"))
  in
  PubSub.publish ~to':(Jid.bare jid) ~node:as_node client (Some item)

let retrieve client jid =
  PubSub.retrieve ~to':(Jid.bare jid) ~node:as_node client
