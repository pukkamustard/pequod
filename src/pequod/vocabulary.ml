(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* The vocabularies used for describing scrobbles and helpers for using such scrobbles. *)

let hash s =
  let _, read_cap =
    Eris.(
      encode_string ~convergence_secret:null_convergence_secret
        ~block_size:`Small s)
  in
  Rdf.Iri.of_string @@ Eris.Read_capability.to_urn read_cap

(* Namespaces *)

(* ActivityStreams *)
let as' = Rdf.Namespace.make_namespace "https://www.w3.org/ns/activitystreams#"

(* The Music Ontology *)
let mo = Rdf.Namespace.make_namespace "http://purl.org/ontology/mo/"

(* DC Terms *)
let dc_terms = Rdf.Namespace.make_namespace "http://purl.org/dc/terms/"

(* FOAF *)
let foaf = Rdf.Namespace.make_namespace "http://xmlns.com/foaf/0.1/"

(* Helpers *)

(* Options galore ... we need a little helper *)
let option_bind f p = Option.bind p f

(* Cast a UUID to an IRI *)
let iri_of_uuid uuid = "urn:uuid:" ^ Uuidm.to_string uuid |> Rdf.Iri.of_string

(* Classes *)

module Track = struct
  type t = {
    title : string option;
    artist : string option;
    musicbrainz_track : Uuidm.t;
    musicbrainz_album : Uuidm.t option;
    musicbrainz_artist : Uuidm.t option;
  }

  let of_mpd_response response =
    Mpd.Response.find_opt "MUSICBRAINZ_TRACKID" response
    (* if no MUSICBRAINZ_TRACKID set use MUSCIBRAINZ_RELEASETRACKID *)
    |> (function
         | None -> Mpd.Response.find_opt "MUSICBRAINZ_RELEASETRACKID" response
         | Some id -> Some id)
    |> option_bind Uuidm.of_string
    |> Option.map (fun musicbrainz_track ->
           {
             title = Mpd.Response.find_opt "Title" response;
             artist = Mpd.Response.find_opt "Artist" response;
             musicbrainz_track;
             musicbrainz_album =
               Mpd.Response.find_opt "MUSICBRAINZ_ALBUMID" response
               |> option_bind Uuidm.of_string;
             musicbrainz_artist =
               Mpd.Response.find_opt "MUSICBRAINZ_ARTISTID" response
               |> option_bind Uuidm.of_string;
           })

  let pp ppf track =
    Fmt.(
      pf ppf "@[%a - %a %a@]" (option string) track.artist (option string)
        track.title
        (styled `Faint @@ parens Uuidm.pp)
        track.musicbrainz_track)
end

module Scrobble = struct
  type t = Rdf_cbor.Content_addressable.t

  let id t = Rdf_cbor.Content_addressable.base_subject ~hash t

  let make ~actor ?(start_time = Ptime_clock.now ()) (track : Track.t) =
    Rdf_cbor.Content_addressable.(
      empty
      |> add_statement
           (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
           (Object.of_iri @@ as' "Listen")
      |> add_statement (Predicate.of_iri @@ as' "actor") (Object.of_iri @@ actor)
      |> add_statement
           (Predicate.of_iri @@ as' "object")
           (Object.make_fragment_reference "track")
      |> add_statement
           (Predicate.of_iri @@ as' "startTime")
           (Object.of_literal
           @@ Rdf.Literal.make
                (Ptime.to_rfc3339 start_time)
                (Rdf.Namespace.xsd "dateTime"))
      (* Add the fragment statements for track *)
      |> add_fragment_statement "track"
           (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
           (Object.of_iri @@ mo "Track")
      |> add_opt_fragment_statement "track"
           (Predicate.of_iri @@ dc_terms "title")
           (Option.map
              (fun title -> Object.of_literal @@ Rdf.Literal.make_string title)
              track.title)
      |> add_opt_fragment_statement "track"
           (Predicate.of_iri @@ dc_terms "creator")
           (Option.map
              (fun title -> Object.of_literal @@ Rdf.Literal.make_string title)
              track.artist)
      |> add_fragment_statement "track"
           (Predicate.of_iri @@ mo "musicbrainz")
           (Object.of_iri @@ iri_of_uuid track.musicbrainz_track)
      |> add_opt_fragment_statement "track"
           (Predicate.of_iri @@ foaf "maker")
           (Option.map
              (fun musicbrainz_artist ->
                Object.of_iri @@ iri_of_uuid musicbrainz_artist)
              track.musicbrainz_artist)
      |> add_opt_fragment_statement "track"
           (Predicate.of_iri @@ mo "release")
           (Option.map
              (fun musicbrainz_album ->
                Object.of_iri @@ iri_of_uuid musicbrainz_album)
              track.musicbrainz_album))

  let xml_prefixes =
    [
      ("as", as' ""); ("mo", mo ""); ("dcterms", dc_terms ""); ("foaf", foaf "");
    ]

  let to_xml listen =
    Rdf.Graph.empty
    |> Rdf.Graph.add_seq (Rdf_cbor.Content_addressable.to_triples ~hash listen)
    |> Rdf_xml.to_signals ~prefixes:xml_prefixes
    (* Oi, oi oi. This is not nice. Xmpp wants a tree wheres Rdf_xml just emits signals.. *)
    |> Lwt_stream.of_seq
    |> Xmlc.Parser.parse_stream Xmlc.Tree.parser
end
