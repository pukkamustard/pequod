(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Archi_lwt

(* Setup logging *)
let src = Logs.Src.create "Pequod"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

let help_secs =
  let open Cmdliner in
  [
    `S Manpage.s_common_options;
    `P "These options are common to all commands.";
    `S "MORE HELP";
    `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";
    `S Manpage.s_authors;
    `P "pukkamustard <pukkamustard@posteo.net>";
    `S "LICENSE";
    `P "AGPL-3.0-or-later";
    `P "The source code is available at https://inqlab.net/git/pequod.git";
  ]

let scrobble config =
  let system = System.make [ ("scrobbler", Scrobbler.component) ] in
  System.start config system >>= function
  | Ok system ->
      let forever, waiter = Lwt.wait () in
      let _sigint_handler =
        Lwt_unix.on_signal Sys.sigint (fun _ ->
            Lwt.async (fun () ->
                Log.app (fun m -> m "received SIGINT, stopping system.")
                >>= fun () ->
                System.stop system >|= fun _ -> Lwt.wakeup_later waiter 0))
      in
      forever
  | Error (`Msg error) ->
      Log.err (fun m -> m "Could not start scrobbling: %s" error) >>= fun () ->
      return 1
  | Error `Cycle_found ->
      Log.err (fun m -> m "Could not start scrobbling: Cycle_found")
      >>= fun () -> return 1

let scrobble_cmd =
  let open Cmdliner in
  let doc = "Listen to songs played on MPD and scrobble to XMPP" in
  Cmd.v (Cmd.info "scrobble" ~doc)
    Term.(
      const (fun options -> Lwt_main.run @@ scrobble options)
      $ Configuration.term)

let get config jid as_rdf_xml =
  let* xmpp = Xmpp.start config in
  match xmpp with
  | Ok xmpp ->
      let* graph = Scrobbler.get_graph ?jid xmpp in
      if as_rdf_xml then (
        let output =
          Xmlm.make_output ~nl:true ~indent:(Some 2) (`Channel stdout)
        in
        Xmlm.output output (`Dtd None);
        graph
        |> Rdf_xml.to_signals ~prefixes:Vocabulary.Scrobble.xml_prefixes
        |> Seq.iter (Xmlm.output output);
        return 0)
      else
        Scrobbler.Graph.listen_activities graph
        |> Lwt_list.iter_s (fun listen ->
               Log.app (fun m ->
                   m "%a" (Scrobbler.Graph.pp_listen graph) listen))
        >>= fun () -> return 0
  | Error (`Msg error) ->
      Log.err (fun m -> m "Could not start scrobbling: %s" error) >>= fun () ->
      return 1
  | Error `Cycle_found ->
      Log.err (fun m -> m "Could not start scrobbling: Cycle_found")
      >>= fun () -> return 1

let get_cmd =
  let open Cmdliner in
  let doc = "Retrieves list of scrobbles from contact and prints them." in
  let jid_term =
    let open Cmdliner in
    let doc =
      "XMPP user to get scrobbles from. If not provided scrobbles will be \
       retrieved from XMPP account used to connect (i.e. from yourself)."
    in
    Term.(
      const (fun s -> Option.bind s Xmpp.Jid.of_string)
      $ Arg.(value & pos 0 (some string) None & info [] ~docv:"JID" ~doc))
  in
  let as_rdf_xml_term =
    let open Cmdliner in
    let doc = "Output Scrobbles as RDF/XML" in
    Arg.(value & flag & info [ "rdf-xml" ] ~doc)
  in
  Cmd.v (Cmd.info "get" ~doc)
    Term.(
      const (fun options jid as_rdf_xml ->
          Lwt_main.run @@ get options jid as_rdf_xml)
      $ Configuration.term $ jid_term $ as_rdf_xml_term)

let cmds = [ scrobble_cmd; get_cmd ]

let main_cmd =
  let open Cmdliner in
  let doc = "Something something music sharing" in
  let sdocs = Manpage.s_common_options in
  let exits = Cmd.Exit.defaults in
  let info = Cmd.info "pequod" ~version:"0.0.0" ~doc ~sdocs ~exits in
  let default =
    Term.(ret (const (fun _ -> `Help (`Pager, None)) $ Configuration.term))
  in
  Cmd.group info ~default cmds

let () =
  let open Cmdliner in
  exit @@ Cmd.eval' main_cmd
