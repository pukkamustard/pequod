# Pequod

Something, something music sharing.

# Overview

Pequod is an experiment into a friend-to-friend music sharing system.

## Scrobble

Pequod can observes what is being played in MPD and creates XMPP posts for every song listened:

```
$ dune exec src/pequod/pequod.exe -- scrobble --jid user@strawberry.local --password pencil -v
pequod.exe: [INFO] Connected to MPD.
pequod.exe: [INFO] Opening TCP socket to 127.0.0.1 (port: 5222; disable_ssl: true)
pequod.exe: [INFO] XMPP Client connected as user@strawberry.local/berwsTgK.
pequod.exe: [INFO] Scrobbling track urn:uuid:a944e6fe-9357-3230-8028-b0c7efe7c58a (Jay Dee - Think Twice) to XMPP.
```

Scrobbles are published as [ActivityStreams Listen](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-listen) activities using [The Music Ontology](http://musicontology.com/specification/) properties. Most importantly, the [MusicBrainz identfier](https://musicbrainz.org/doc/MusicBrainz_Identifier) of the track is included.

See [vocabulary.md](doc/vocabulary.md) for details on the data model and vocabularies used.

## Get Scrobbles

To get scrobbles:

```
$ dune exec src/pequod/pequod.exe -- get --jid user@strawberry.local --password pencil 
xmpp:user@strawberry.local (2022-01-20 11:31:11 +00:00): Jay Dee - Think Twice (urn:uuid:a944e6fe-9357-3230-8028-b0c7efe7c58a)
```

Of course this is ugly. A nicer UI will eventually come...

You can also get scrobbles from other users (who share their presence with you) with `pequod get friend@some-xmpp.org`.

See [this blog post](https://inqlab.net/2022-01-17-activitystreams-over-xmpp.html) for details on how this works.

WARNING: Pequod will re-configure the PEP node `net.openengiadina.xmpp.activitystreams` to persist 256 items. By default only a single item is persisted on most XMPP servers. In the future this should be less forcefull and configurable.

# TODOs and Ideas

## A nice UI for viewing scrobbles

Uploading scrobbles is not really useful if there is no way to view them. We need a little client UI that can be used to view your own and your friends scrobbles. Would be nice to have things like weekly/monthly statistics and such.

## MPRIS

[MPRIS](https://specifications.freedesktop.org/mpris-spec/latest/) is a D-Bus interface for media players. It can be used to get information on what tracks are being played.

It seems that players do not expose the Musicbrainz metadata via MPRIS, even if the audio files are properly tagged. MPRIS does seem to expose the location of the audio file, so we could get the missing tags by looking at the file directly. This does require using something like [Taglib](https://taglib.org/).

Initial experiments with [ocaml-taglib](https://github.com/savonet/ocaml-taglib) failed, as the Musicbrainz tags seems to be in ID3v2 frames or Vorbis metadata that does not seem to be exposed in the OCaml bindings.

## ERIS

Add the [ERIS](http://purl.org/eris) read-capability of the audio file to the scrobbled track. Eventually also make the blocks available and allow sharing of music (and not only music metadata).


# Hacking

A suitable development environment can be set up with [Guix](https://guix.gnu.org):

```
guix shell -D -f guix.scm
```

This is currently the easiest way to get hacking as many OCaml dependencies have not yet been published to OPAM.

Use `dune exec src/pequod/pequod.exe` to run Pequod.


# Inspriation and Related Projects

## [DBTune.org](http://dbtune.org/last-fm/)

Provided an RDF interface to last.fm/Audioscrobbler data.

They use the Music Ontology, FOAF and a simple ontology for describing Scrobbles based on the [Event Ontology](http://motools.sourceforge.net/event/event.html).

## [mpris-scrobbler](https://github.com/mariusor/mpris-scrobbler)

A scrobbler that listens for media players on MPRIS. 


## Contact

pukkamustard [at] posteo [dot] net


## License

[AGPL-3.0-or-later](./LICENSE/AGPL-3.0-or-later.txt)
